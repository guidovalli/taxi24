package com.thebridge.taxi24.infrastructure.repository;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.domain.model.Passenger;
import com.thebridge.taxi24.domain.repository.PassengerRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PassengerJpaRepository extends JpaRepository<Passenger, Long>, PassengerRepository {

    @Override
    @SuppressWarnings("unchecked")
    Passenger save(Passenger passenger);

    @Override
    List<Passenger> findAll();

    @Override
    Optional<Passenger> findById(@NotNull Long id);

    @Override
    List<Passenger> findByStatus(@NotNull Long statusId);

}
package com.thebridge.taxi24.infrastructure.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.domain.model.Driver;
import com.thebridge.taxi24.domain.model.Location;
import com.thebridge.taxi24.domain.repository.DriverRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DriverJpaRepository extends JpaRepository<Driver, Long>, DriverRepository {

    @Override
    @SuppressWarnings("unchecked")
    Driver save(Driver driver);

    @Override
    List<Driver> findAll();

    @Override
    Optional<Driver> findById(@NotNull Long id);

    @Override
    List<Driver> findByStatus(@NotNull Integer statusId);



    //Metodo que simula la obtención de conductores cercanos disponibles (STATUS_AVAILABLE), random de 3 elementos que no se repiten
    @Override
    default List<Driver> findByLocationLessOrEquals(Location location){
        Random rand = new Random();
        List<Driver> givenList = findByStatus(Driver.STATUS_AVAILABLE);
    
        int numberOfElements = 3;
    
        List<Driver> selectedDrivers = new ArrayList<>();
        for (int i = 0; i < numberOfElements; i++) {
            int randomIndex = rand.nextInt(givenList.size());
            selectedDrivers.add(givenList.get(randomIndex));
            givenList.remove(randomIndex);
        }
        return selectedDrivers;
    }


}
package com.thebridge.taxi24.infrastructure.repository;

import com.thebridge.taxi24.domain.model.Invoice;
import com.thebridge.taxi24.domain.model.Passenger;
import com.thebridge.taxi24.domain.repository.InvoiceRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceJpaRepository extends JpaRepository<Passenger, Long>, InvoiceRepository {

    @Override
    @SuppressWarnings("unchecked")
    Invoice save(Invoice invoice);

}
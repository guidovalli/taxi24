package com.thebridge.taxi24.infrastructure.repository;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.domain.model.Travel;
import com.thebridge.taxi24.domain.repository.TravelRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TravelJpaRepository extends JpaRepository<Travel, Long>, TravelRepository {

    @Override
    @SuppressWarnings("unchecked")
    Travel save(Travel travel);

    @Override
    List<Travel> findByStatus(@NotNull Integer statusId);

}
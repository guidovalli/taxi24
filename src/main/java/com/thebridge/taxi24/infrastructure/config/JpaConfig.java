package com.thebridge.taxi24.infrastructure.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = {"com.thebridge.taxi24.infrastructure.repository"})
public class JpaConfig {}
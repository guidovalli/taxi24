package com.thebridge.taxi24.application.dto.response;

import java.text.ParseException;

import com.thebridge.taxi24.domain.model.Location;
import com.thebridge.taxi24.domain.model.Passenger;

import org.modelmapper.ModelMapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PassengerResponseDto {

    private Long id;

    private Integer status;

    private Location location;

    private static final ModelMapper modelMapper = new ModelMapper();

    public static PassengerResponseDto of(Passenger passenger) {
        PassengerResponseDto responseDto = modelMapper.map(passenger, PassengerResponseDto.class);
        // Map or transform another field like dates


        return responseDto;
    }

    public static Passenger convertToEntity(PassengerResponseDto responseDto) throws ParseException {
        Passenger passenger = modelMapper.map(responseDto, Passenger.class);

        // Map or transform another field like dates
        return passenger;
    }
}

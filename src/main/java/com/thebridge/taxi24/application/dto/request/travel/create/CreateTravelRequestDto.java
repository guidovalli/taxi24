package com.thebridge.taxi24.application.dto.request.travel.create;

import java.text.ParseException;

import com.thebridge.taxi24.domain.model.Driver;
import com.thebridge.taxi24.domain.model.Passenger;
import com.thebridge.taxi24.domain.model.Travel;

import org.modelmapper.ModelMapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateTravelRequestDto {

    private Passenger passenger;

    private Driver driver;

    private static final ModelMapper modelMapper = new ModelMapper();

    public static CreateTravelRequestDto of(Travel travel) {
        CreateTravelRequestDto requestDto = modelMapper.map(travel, CreateTravelRequestDto.class);
        // Map or transform another field like dates

        return requestDto;
    }

    public static Travel convertToEntity(CreateTravelRequestDto requestDto) throws ParseException {
        Travel travel = modelMapper.map(requestDto, Travel.class);

        // Map or transform another field like dates
        return travel;
    }
}

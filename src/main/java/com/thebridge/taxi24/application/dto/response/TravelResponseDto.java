package com.thebridge.taxi24.application.dto.response;

import java.text.ParseException;

import com.thebridge.taxi24.domain.model.Driver;
import com.thebridge.taxi24.domain.model.Invoice;
import com.thebridge.taxi24.domain.model.Passenger;
import com.thebridge.taxi24.domain.model.Travel;

import org.modelmapper.ModelMapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TravelResponseDto {

    private Long id;

    private Integer status;

    private Passenger passenger;

    private Driver driver;

    private Invoice invoice;

    private static final ModelMapper modelMapper = new ModelMapper();

    public static TravelResponseDto of(Travel travel) {
        TravelResponseDto responseDto = modelMapper.map(travel, TravelResponseDto.class);
        // Map or transform another field like dates

        return responseDto;
    }

    public static Travel convertToEntity(TravelResponseDto responseDto) throws ParseException {
        Travel travel = modelMapper.map(responseDto, Travel.class);

        // Map or transform another field like dates
        return travel;
    }
}

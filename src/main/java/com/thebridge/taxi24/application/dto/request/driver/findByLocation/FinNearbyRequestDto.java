package com.thebridge.taxi24.application.dto.request.driver.findByLocation;

import java.text.ParseException;

import com.thebridge.taxi24.domain.model.Location;

import org.modelmapper.ModelMapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FinNearbyRequestDto {

    private double latitud;
    
    private double longitud;

    private static final ModelMapper modelMapper = new ModelMapper();

    public static FinNearbyRequestDto of(Location location) {
        FinNearbyRequestDto requestDto = modelMapper.map(location, FinNearbyRequestDto.class);
        // Map or transform another field like dates

        return requestDto;
    }

    public static Location convertToEntity(FinNearbyRequestDto requestDto) throws ParseException {
        Location location = modelMapper.map(requestDto, Location.class);

        // Map or transform another field like dates
        return location;
    }
}

package com.thebridge.taxi24.application.dto.response;

import java.math.BigDecimal;
import java.text.ParseException;

import com.thebridge.taxi24.domain.model.Invoice;
import com.thebridge.taxi24.domain.model.Travel;

import org.modelmapper.ModelMapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoiceDto {

    private Long id;

    private BigDecimal total;

    private Travel travel;

    private static final ModelMapper modelMapper = new ModelMapper();

    public static InvoiceDto of(Invoice invoice) {
        InvoiceDto responseDto = modelMapper.map(invoice, InvoiceDto.class);
        // Map or transform another field like dates


        return responseDto;
    }

    public static Invoice convertToEntity(InvoiceDto responseDto) throws ParseException {
        Invoice invoice = modelMapper.map(responseDto, Invoice.class);

        // Map or transform another field like dates
        return invoice;
    }
}
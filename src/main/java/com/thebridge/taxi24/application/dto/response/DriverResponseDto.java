package com.thebridge.taxi24.application.dto.response;

import java.text.ParseException;

import com.thebridge.taxi24.domain.model.Driver;

import org.modelmapper.ModelMapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DriverResponseDto {

    private Long id;

    private Integer status;

    private LocationResponseDto location;

    private static final ModelMapper modelMapper = new ModelMapper();


    public static DriverResponseDto of(Driver driver) {
        DriverResponseDto dto = modelMapper.map(driver, DriverResponseDto.class);
        //Map or transform another field like dates

        return dto;
    }

    public static Driver convertToEntity(DriverResponseDto driverResponseDto) throws ParseException {
        Driver entity = modelMapper.map(driverResponseDto, Driver.class);

        //Map or transform another field like dates
        return entity;
    }
}

package com.thebridge.taxi24.application.dto.response;

import java.text.ParseException;

import com.thebridge.taxi24.domain.model.Location;

import org.modelmapper.ModelMapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LocationResponseDto {

    private Long id;

    private double latitud;
    
    private double longitud;

    private static final ModelMapper modelMapper = new ModelMapper();

    public static LocationResponseDto of(Location location) {
        LocationResponseDto dto = modelMapper.map(location, LocationResponseDto.class);
        //Map or transform another field like dates

        return dto;
    }

    public static Location convertToEntity(LocationResponseDto locationResponseDto) throws ParseException {
        Location entity = modelMapper.map(locationResponseDto, Location.class);

        //Map or transform another field like dates
        return entity;
    }
}

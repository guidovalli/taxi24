package com.thebridge.taxi24.application.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Set;

import javax.validation.Valid;

import com.thebridge.taxi24.application.dto.request.travel.create.CreateTravelRequestDto;
import com.thebridge.taxi24.application.dto.response.TravelResponseDto;
import com.thebridge.taxi24.application.service.driver.DriverService;
import com.thebridge.taxi24.application.service.driver.DriverServiceImpl;
import com.thebridge.taxi24.application.service.passenger.PassengerService;
import com.thebridge.taxi24.application.service.passenger.PassengerServiceImpl;
import com.thebridge.taxi24.application.service.travel.TravelService;
import com.thebridge.taxi24.application.service.travel.TravelServiceImpl;
import com.thebridge.taxi24.application.service.travel.invoice.InvoiceService;
import com.thebridge.taxi24.application.service.travel.invoice.InvoiceServiceImpl;
import com.thebridge.taxi24.domain.model.Travel;
import com.thebridge.taxi24.domain.repository.DriverRepository;
import com.thebridge.taxi24.domain.repository.InvoiceRepository;
import com.thebridge.taxi24.domain.repository.PassengerRepository;
import com.thebridge.taxi24.domain.repository.TravelRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/travel")
public class TravelController {

	private TravelService travelService;

	@Autowired
	public TravelController(TravelRepository travelRepository, PassengerRepository passengerRepository, DriverRepository driverRepository, InvoiceRepository invoiceRepository) {
		DriverService driverService = new DriverServiceImpl(driverRepository);
		PassengerService passengerService = new PassengerServiceImpl(passengerRepository, driverRepository);
		InvoiceService invoiceService = new InvoiceServiceImpl(invoiceRepository);

		this.travelService = new TravelServiceImpl(travelRepository, passengerService, driverService, invoiceService);
	}


	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public TravelResponseDto create(@Valid @RequestBody CreateTravelRequestDto travel) throws ParseException{		
		return travelService.create(travel);
	}

	// @RequestMapping(value = "/{status}", method = RequestMethod.GET)
	// @ResponseStatus(HttpStatus.OK)
	// public Set<TravelResponseDto> findBystatus(@PathVariable("status") Integer statusId) {
	// 	return travelService.findByStatus(statusId);
	// }


	@RequestMapping(value="active", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Set<TravelResponseDto> findAllActive() {
		return travelService.findByStatus(Travel.STATUS_ACTIVE);
	}



	@RequestMapping(value="/{travelId}/complete", params = {"total"}, method = RequestMethod.PATCH)
	@ResponseStatus(HttpStatus.OK)
	public void complete(
		@PathVariable("travelId") Long travelId,
		@RequestParam(value = "total", required = true) BigDecimal total		
	) throws ParseException {
		travelService.complete(travelId, total);
	}
}

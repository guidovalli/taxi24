package com.thebridge.taxi24.application.controller;

import java.util.Set;

import com.thebridge.taxi24.application.dto.response.DriverResponseDto;
import com.thebridge.taxi24.application.service.driver.DriverService;
import com.thebridge.taxi24.application.service.driver.DriverServiceImpl;
import com.thebridge.taxi24.domain.model.Driver;
import com.thebridge.taxi24.domain.repository.DriverRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/driver")
public class DriverController {

	private DriverService driverService;

	@Autowired
	public DriverController(DriverRepository driverRepository) {
		this.driverService = new DriverServiceImpl(driverRepository);
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Set<DriverResponseDto> findAll() {
		return driverService.findAll();
	}


	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public DriverResponseDto findById(@PathVariable("id") Long id) throws Exception {;
		return driverService.findById(id);
	}



	@RequestMapping(value="available", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Set<DriverResponseDto> findAllAvailable() {
		return driverService.findByStatus(Driver.STATUS_AVAILABLE);
	}


	@RequestMapping( params = {"latitud","longitud"}, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Set<DriverResponseDto> findNearby(
		@RequestParam(value = "latitud", required = true) double latitud,
		@RequestParam(value = "longitud", required = true) double longitud
	) {
		return driverService.findNearby(latitud, longitud);
	}


}

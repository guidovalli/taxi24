package com.thebridge.taxi24.application.controller;

import java.util.Set;

import com.thebridge.taxi24.application.dto.response.DriverResponseDto;
import com.thebridge.taxi24.application.dto.response.PassengerResponseDto;
import com.thebridge.taxi24.application.service.passenger.PassengerService;
import com.thebridge.taxi24.application.service.passenger.PassengerServiceImpl;
import com.thebridge.taxi24.domain.repository.DriverRepository;
import com.thebridge.taxi24.domain.repository.PassengerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/passenger")
public class PassengerController {

	private PassengerService passengerService;


	@Autowired
	public PassengerController(PassengerRepository passengerRepository, DriverRepository driverRepository) {
		this.passengerService = new PassengerServiceImpl(passengerRepository, driverRepository);
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Set<PassengerResponseDto> findAll() {
		return passengerService.findAll();
	}


	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public PassengerResponseDto findById(@PathVariable("id") Long id) throws Exception {;
		return passengerService.findById(id);
	}



	@RequestMapping( value = "/{id}/nearby_drivers", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Set<DriverResponseDto> findNearbyDrivers(
		@PathVariable("id") Long id
	) {
		return passengerService.findNearbyDrivers(id);
	}
}

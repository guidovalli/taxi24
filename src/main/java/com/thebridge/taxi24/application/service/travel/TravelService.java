package com.thebridge.taxi24.application.service.travel;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.application.dto.request.travel.create.CreateTravelRequestDto;
import com.thebridge.taxi24.application.dto.response.TravelResponseDto;

import org.springframework.validation.annotation.Validated;

@Validated
public interface TravelService {

	public Set<TravelResponseDto> findByStatus(@NotNull Integer statusId);

	public void complete(Long travelId, BigDecimal total) throws ParseException;

	public TravelResponseDto findById(@NotNull Long id);

    public TravelResponseDto create(@Valid CreateTravelRequestDto travel) throws ParseException;

}

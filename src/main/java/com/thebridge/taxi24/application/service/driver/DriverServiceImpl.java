package com.thebridge.taxi24.application.service.driver;

import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.application.dto.response.DriverResponseDto;
import com.thebridge.taxi24.domain.model.Location;
import com.thebridge.taxi24.domain.repository.DriverRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DriverServiceImpl implements DriverService {

	private DriverRepository driverRepository;


	@Autowired
	public DriverServiceImpl(DriverRepository driverRepository) {
		this.driverRepository = driverRepository;

	}


	@Override
	public DriverResponseDto findById(@NotNull Long id) {
		return DriverResponseDto.of(
			driverRepository.findById(id)
			.orElseThrow(() -> new EntityNotFoundException("Driver not found with ID " + id))
		);
	}

	@Override
	public Set<DriverResponseDto> findAll() {
		return driverRepository.findAll().stream()
		.map(driver-> DriverResponseDto.of(driver))
		.collect(Collectors.toSet());
	}


	@Override
	public Set<DriverResponseDto> findByStatus(Integer statusId) {
		return driverRepository.findByStatus(statusId).stream()
		.map(driver-> DriverResponseDto.of(driver))
		.collect(Collectors.toSet());
	}


	@Override
	public Set<DriverResponseDto> findNearby(double latitud, double longitud) {
		Location location = new Location();
		location.setLatitud(latitud);
		location.setLongitud(longitud);

		return driverRepository.findByLocationLessOrEquals(location).stream()
		.map(driver-> DriverResponseDto.of(driver))
		.collect(Collectors.toSet());
	}

}

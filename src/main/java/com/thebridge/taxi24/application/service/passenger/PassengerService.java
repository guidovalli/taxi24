package com.thebridge.taxi24.application.service.passenger;

import java.util.Set;

import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.application.dto.response.DriverResponseDto;
import com.thebridge.taxi24.application.dto.response.PassengerResponseDto;
import com.thebridge.taxi24.domain.model.Location;

import org.springframework.validation.annotation.Validated;

@Validated
public interface PassengerService {

	public PassengerResponseDto findById(@NotNull Long id);

	public Set<PassengerResponseDto> findAll();

	public PassengerResponseDto findByIdOrCreate(@NotNull Long id, Location location);

	public Set<DriverResponseDto> findNearbyDrivers(Long id);
	
}

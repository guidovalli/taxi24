package com.thebridge.taxi24.application.service.travel.invoice;

import java.math.BigDecimal;
import java.text.ParseException;

import com.thebridge.taxi24.application.dto.response.InvoiceDto;
import com.thebridge.taxi24.application.dto.response.TravelResponseDto;

import org.springframework.validation.annotation.Validated;

@Validated
public interface InvoiceService {

	public InvoiceDto create(TravelResponseDto travel, BigDecimal total) throws ParseException;

}

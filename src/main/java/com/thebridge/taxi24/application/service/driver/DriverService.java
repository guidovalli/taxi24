package com.thebridge.taxi24.application.service.driver;

import java.util.Set;

import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.application.dto.response.DriverResponseDto;

import org.springframework.validation.annotation.Validated;

@Validated
public interface DriverService {

	public DriverResponseDto findById(@NotNull Long id);

	public Set<DriverResponseDto> findAll();

    public Set<DriverResponseDto> findByStatus(Integer statusId);

    public Set<DriverResponseDto> findNearby(double latitud, double longitud);

}

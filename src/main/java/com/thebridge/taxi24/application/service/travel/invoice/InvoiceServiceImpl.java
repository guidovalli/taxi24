package com.thebridge.taxi24.application.service.travel.invoice;

import java.math.BigDecimal;
import java.text.ParseException;

import com.thebridge.taxi24.application.dto.response.InvoiceDto;
import com.thebridge.taxi24.application.dto.response.TravelResponseDto;
import com.thebridge.taxi24.domain.model.Invoice;
import com.thebridge.taxi24.domain.repository.InvoiceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class InvoiceServiceImpl implements InvoiceService {

	private InvoiceRepository invoiceRepository;



	@Autowired
	public InvoiceServiceImpl(InvoiceRepository invoiceRepository) {
		this.invoiceRepository = invoiceRepository;
	}


	@Override
	public InvoiceDto create(TravelResponseDto travel, BigDecimal total) throws ParseException {

		Invoice invoice = new Invoice();
		invoice.setTotal(total);
		invoice.setTravel(TravelResponseDto.convertToEntity(travel));
		invoiceRepository.save(invoice);

		return InvoiceDto.of(invoice);
	}

}

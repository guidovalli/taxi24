package com.thebridge.taxi24.application.service.passenger;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.application.dto.response.DriverResponseDto;
import com.thebridge.taxi24.application.dto.response.PassengerResponseDto;
import com.thebridge.taxi24.application.service.driver.DriverService;
import com.thebridge.taxi24.application.service.driver.DriverServiceImpl;
import com.thebridge.taxi24.domain.model.Location;
import com.thebridge.taxi24.domain.model.Passenger;
import com.thebridge.taxi24.domain.repository.DriverRepository;
import com.thebridge.taxi24.domain.repository.PassengerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PassengerServiceImpl implements PassengerService {

	private PassengerRepository passengerRepository;
	private DriverService driverService;

	@Autowired
	public PassengerServiceImpl(PassengerRepository passengerRepository, DriverRepository driverRepository) {
		this.passengerRepository = passengerRepository;
		this.driverService = new DriverServiceImpl(driverRepository);

	}


	@Override
	public PassengerResponseDto findById(@NotNull Long id) {
		return PassengerResponseDto.of(
			passengerRepository.findById(id)
			.orElseThrow(() -> new EntityNotFoundException("Passenger not found with ID " + id))
		);
	}

	@Override
	public Set<PassengerResponseDto> findAll() {
		return passengerRepository.findAll().stream()
		.map(passenger-> PassengerResponseDto.of(passenger))
		.collect(Collectors.toSet());
	}


	@Override
	public PassengerResponseDto findByIdOrCreate(@NotNull Long id, Location location) {
		Optional<Passenger> passengerOpt = passengerRepository.findById(id);

		if(passengerOpt.isEmpty()) return PassengerResponseDto.of(create(id, location));

		return PassengerResponseDto.of(
			passengerOpt.get()
		);	
	}


	private Passenger create(@NotNull Long id, @NotNull Location location) {
		passengerRepository.findById(id).ifPresent((pass) -> {
			throw new EntityExistsException("Passenger exist with ID " + pass.getId());
		});

		Passenger passenger = new Passenger();
		passenger.setId(id);
		passenger.setStatus(Passenger.STATUS_NO_TRAVEL);

		passenger.setLocation(location);

		return passengerRepository.save(passenger);
	}


	@Override
	public Set<DriverResponseDto> findNearbyDrivers(Long id) {
		PassengerResponseDto passenger = findById(id);
		
		return  driverService.findNearby(
			passenger.getLocation().getLatitud(), 
			passenger.getLocation().getLongitud()
		);
	}



}

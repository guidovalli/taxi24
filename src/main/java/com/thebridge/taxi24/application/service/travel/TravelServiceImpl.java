package com.thebridge.taxi24.application.service.travel;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.application.dto.request.travel.create.CreateTravelRequestDto;
import com.thebridge.taxi24.application.dto.response.DriverResponseDto;
import com.thebridge.taxi24.application.dto.response.InvoiceDto;
import com.thebridge.taxi24.application.dto.response.PassengerResponseDto;
import com.thebridge.taxi24.application.dto.response.TravelResponseDto;
import com.thebridge.taxi24.application.service.driver.DriverService;
import com.thebridge.taxi24.application.service.passenger.PassengerService;
import com.thebridge.taxi24.application.service.travel.invoice.InvoiceService;
import com.thebridge.taxi24.domain.model.Invoice;
import com.thebridge.taxi24.domain.model.Passenger;
import com.thebridge.taxi24.domain.model.Travel;
import com.thebridge.taxi24.domain.repository.TravelRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TravelServiceImpl implements TravelService {

	private TravelRepository travelRepository;

	private DriverService driverService;
	
	private PassengerService passengerService;

	private InvoiceService invoiceService;


	@Autowired
	public TravelServiceImpl(TravelRepository travelRepository, PassengerService passengerService, DriverService driverService, InvoiceService invoiceService) {
		this.travelRepository = travelRepository;
		this.passengerService = passengerService;
		this.driverService = driverService;
		this.invoiceService = invoiceService;
	}

	@Override
	public TravelResponseDto findById(@NotNull Long id) {

		return TravelResponseDto.of(travelRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("Travel not found with ID " + id)));
	}

	@Override
	public Set<TravelResponseDto> findByStatus(@NotNull Integer statusId) {
		return travelRepository.findByStatus(statusId).stream().map(travel -> TravelResponseDto.of(travel))
				.collect(Collectors.toSet());
	}

	@Override
	public void complete(Long travelId, BigDecimal total) throws ParseException {
		TravelResponseDto travelResponseDto = findById(travelId);

		Invoice invoice = InvoiceDto.convertToEntity(invoiceService.create(travelResponseDto, total));

		Travel travel = TravelResponseDto.convertToEntity(travelResponseDto);
		travel.setInvoice(invoice);
		travel.setStatus(Travel.STATUS_COMPLETED);
		travelRepository.save(travel);
	}

	@Override
	public TravelResponseDto create(@Valid CreateTravelRequestDto travelRequest) throws ParseException {
		DriverResponseDto driver = driverService.findById(travelRequest.getDriver().getId());
		PassengerResponseDto passenger = passengerService.findByIdOrCreate(travelRequest.getPassenger().getId(), travelRequest.getPassenger().getLocation());
		passenger.setStatus(Passenger.STATUS_IN_TRAVEL);

		Travel travel = CreateTravelRequestDto.convertToEntity(travelRequest);
		travel.setStatus(Travel.STATUS_ACTIVE);
		travel.setDriver(DriverResponseDto.convertToEntity(driver));
		travel.setPassenger(PassengerResponseDto.convertToEntity(passenger));

		return TravelResponseDto.of(travelRepository.save(travel));
	}

}

package com.thebridge.taxi24.domain.repository;

import java.util.Optional;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.domain.model.Passenger;


public interface PassengerRepository {
    <S extends Passenger> S save(@NotNull S passenger);

    List<Passenger> findAll();

    Optional<Passenger> findById(@NotNull Long id);

    List<Passenger> findByStatus(@NotNull Long statusId);

}
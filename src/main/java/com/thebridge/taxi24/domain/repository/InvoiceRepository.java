package com.thebridge.taxi24.domain.repository;

import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.domain.model.Invoice;

public interface InvoiceRepository {
    <S extends Invoice> S save(@NotNull S invoice);

}

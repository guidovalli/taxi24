package com.thebridge.taxi24.domain.repository;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.domain.model.Travel;


public interface TravelRepository {
    <S extends Travel> S save(@NotNull S travel);

    List<Travel> findByStatus(@NotNull Integer statusId);

    Optional<Travel> findById(@NotNull Long id);
}
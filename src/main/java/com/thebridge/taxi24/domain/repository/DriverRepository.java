package com.thebridge.taxi24.domain.repository;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.thebridge.taxi24.domain.model.Driver;
import com.thebridge.taxi24.domain.model.Location;

public interface DriverRepository {
    <S extends Driver> S save(@NotNull S driver);

    List<Driver> findAll();

    Optional<Driver> findById(@NotNull Long id);

    List<Driver> findByStatus(@NotNull Integer statusId);

    List<Driver> findByLocationLessOrEquals(Location location);

}
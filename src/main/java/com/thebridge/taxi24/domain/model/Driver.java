package com.thebridge.taxi24.domain.model;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@PrimaryKeyJoinColumn(name = "DRIVER_ID")
public class Driver extends Person {
    
	public static final Integer STATUS_AVAILABLE = 1;
	public static final Integer STATUS_BUSSY= 2;

    private Integer status;

}

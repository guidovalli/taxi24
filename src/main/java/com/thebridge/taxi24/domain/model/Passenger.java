package com.thebridge.taxi24.domain.model;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@PrimaryKeyJoinColumn(name = "PASSENGER_ID")
public class Passenger extends Person {

    public static final Integer STATUS_IN_TRAVEL = 1;
    public static final Integer STATUS_NO_TRAVEL = 2;

    private Integer status;
}

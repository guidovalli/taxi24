package com.thebridge.taxi24.domain.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Travel {

    public static final Integer STATUS_ACTIVE = 1;
    public static final Integer STATUS_COMPLETED = 2;
    public static final Integer STATUS_CANCELLED = 3;

    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)	
    private Long id;

    private Integer status;

    @JoinColumn(name="PASSENGER_ID")
    @OneToOne private Passenger passenger;

    @JoinColumn(name="DRIVER_ID")
    @OneToOne private Driver driver;

    @OneToOne private Invoice invoice;
}

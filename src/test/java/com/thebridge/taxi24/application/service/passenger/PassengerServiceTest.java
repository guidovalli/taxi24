package com.thebridge.taxi24.application.service.passenger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;

import com.thebridge.taxi24.application.dto.response.PassengerResponseDto;
import com.thebridge.taxi24.infrastructure.config.config.JpaTestConfig;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;

@DataJpaTest
@Import(JpaTestConfig.class)
public class PassengerServiceTest {
   
	@Autowired
	private PassengerService passengerService;


	@Test
	void testFindById() {
		// Execute the service call
		PassengerResponseDto passenger = passengerService.findById(2L);

		// Assert the response
		assertTrue((passenger!=null), "Passenger was not found");
	}
	
    @Test
	void testFindAll() {
		// Execute the service call
		Set<PassengerResponseDto> passengers = passengerService.findAll();

		// Assert the response
		assertTrue((passengers!=null), "Passengers was not found");
        assertEquals(passengers.size(), 5, "there are more or less Passengers than expected");

	}

}

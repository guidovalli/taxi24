package com.thebridge.taxi24.application.service.travel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Set;

import com.thebridge.taxi24.application.dto.request.travel.create.CreateTravelRequestDto;
import com.thebridge.taxi24.application.dto.response.DriverResponseDto;
import com.thebridge.taxi24.application.dto.response.PassengerResponseDto;
import com.thebridge.taxi24.application.dto.response.TravelResponseDto;
import com.thebridge.taxi24.application.service.driver.DriverService;
import com.thebridge.taxi24.application.service.passenger.PassengerService;
import com.thebridge.taxi24.domain.model.Travel;
import com.thebridge.taxi24.infrastructure.config.config.JpaTestConfig;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;

@DataJpaTest
@Import(JpaTestConfig.class)
public class TravelServiceTest {
    
	@Autowired
	private TravelService travelService;

	@Autowired
	private PassengerService passengerService;
	
	@Autowired
	private DriverService driverService;


	private final Long travelId = 3L;

    @Test
	void testFindAllActive() {
		// Execute the service call
		Set<TravelResponseDto> travels = travelService.findByStatus(Travel.STATUS_ACTIVE);

		// Assert the response
		assertTrue((travels!=null), "Travels was not found");
        assertEquals(travels.size(), 2, "there are more or less Travels than expected");

	}


	@Test
	void testCreateOk() throws ParseException {

		PassengerResponseDto passenger = passengerService.findById(4L);

		DriverResponseDto driver = driverService.findById(6L);

		Travel travel = Travel.builder()
		.driver(DriverResponseDto.convertToEntity(driver))
		.passenger(PassengerResponseDto.convertToEntity(passenger))
		.build();
		
		// Execute the service call
		TravelResponseDto travelResponseDto = travelService.create(CreateTravelRequestDto.of(travel));

		// Assert the response
		assertTrue((travelResponseDto!=null), "Travels was not found");
        assertEquals(travelResponseDto.getId(), travelId, "Expected travel ID is incorrect");
		assertEquals(travelResponseDto.getPassenger().getId(), passenger.getId(), "Expected passenger travel ID is incorrect");
		assertEquals(travelResponseDto.getDriver().getId(), driver.getId(), "Expected drive travel ID is incorrect");
		assertEquals(travelResponseDto.getStatus(), Travel.STATUS_ACTIVE, "Expected travel status is incorrect");

	}


	@Test
	void testCompleteOk() throws ParseException {
		BigDecimal invoiceTotal = new BigDecimal(100);

		// Execute the service call
		travelService.complete(2L, invoiceTotal);

		TravelResponseDto travelResponseDto = travelService.findById(2L);

		// Assert the response
		assertTrue((travelResponseDto!=null), "Travels was not found");
		assertTrue((travelResponseDto.getInvoice()!=null), "Travels Invoice was not found");
		assertEquals(travelResponseDto.getInvoice().getTotal(), invoiceTotal, "Expected travel invoice total is incorrect");
		assertEquals(travelResponseDto.getStatus(), Travel.STATUS_COMPLETED, "Expected travel status is incorrect");

	}


}

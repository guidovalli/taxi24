package com.thebridge.taxi24.application.service.driver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;

import com.thebridge.taxi24.application.dto.response.DriverResponseDto;
import com.thebridge.taxi24.domain.model.Driver;
import com.thebridge.taxi24.infrastructure.config.config.JpaTestConfig;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;

@DataJpaTest
@Import(JpaTestConfig.class)
public class DriverServiceTest {

	@Autowired
	private DriverService driverService;


	@Test
	void testFindById() {
		// Execute the service call
		DriverResponseDto driver = driverService.findById(6L);

		// Assert the response
		assertTrue((driver!=null), "Driver was not found");
	}
	
    @Test
	void testFindAll() {
		// Execute the service call
		Set<DriverResponseDto> drivers = driverService.findAll();

		// Assert the response
		assertTrue((drivers!=null), "Drivers was not found");
        assertEquals(drivers.size(), 5, "there are more or less Drivers than expected");

	}

    @Test
	void testFindAllAvailable() {
		// Execute the service call
		Set<DriverResponseDto> drivers = driverService.findByStatus(Driver.STATUS_AVAILABLE);

		// Assert the response
		assertTrue((drivers!=null), "Drivers was not found");
        assertEquals(drivers.size(), 3, "there are more or less Drivers than expected");

	}

}

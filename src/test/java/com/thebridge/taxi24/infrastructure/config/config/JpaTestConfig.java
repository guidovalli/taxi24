package com.thebridge.taxi24.infrastructure.config.config;


import com.thebridge.taxi24.application.service.driver.DriverService;
import com.thebridge.taxi24.application.service.driver.DriverServiceImpl;
import com.thebridge.taxi24.application.service.passenger.PassengerService;
import com.thebridge.taxi24.application.service.passenger.PassengerServiceImpl;
import com.thebridge.taxi24.application.service.travel.TravelService;
import com.thebridge.taxi24.application.service.travel.TravelServiceImpl;
import com.thebridge.taxi24.application.service.travel.invoice.InvoiceService;
import com.thebridge.taxi24.application.service.travel.invoice.InvoiceServiceImpl;
import com.thebridge.taxi24.domain.repository.DriverRepository;
import com.thebridge.taxi24.domain.repository.InvoiceRepository;
import com.thebridge.taxi24.domain.repository.PassengerRepository;
import com.thebridge.taxi24.domain.repository.TravelRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class JpaTestConfig {

    private final DriverRepository driverRepository;

    private final PassengerRepository passengerRepository;

    private final TravelRepository travelRepository;

    private InvoiceRepository invoiceRepository;


    @Autowired
    public JpaTestConfig(
        DriverRepository driverRepository,
        PassengerRepository passengerRepository,
        TravelRepository travelRepository,
        InvoiceRepository invoiceRepository
        ) {
        this.driverRepository = driverRepository;
        this.passengerRepository = passengerRepository;
        this.travelRepository = travelRepository;
        this.invoiceRepository = invoiceRepository;
    }

    @Bean
    public DriverService driverServiceTestBean() {
        return new DriverServiceImpl(driverRepository);
    }

    @Bean
    public PassengerService passengerServiceTestBean() {
        return new PassengerServiceImpl(passengerRepository, driverRepository);
    }

    @Bean
    public InvoiceService invoiceServiceTestBean() {
        return new InvoiceServiceImpl(invoiceRepository);
    }

    @Bean
    public TravelService travelServiceTestBean() {
        return new TravelServiceImpl(travelRepository, passengerServiceTestBean(), driverServiceTestBean(), invoiceServiceTestBean());
    }
}
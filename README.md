REQUERIMIENTOS Y LIBRERIAS USADAS
- Java 11
- Maven
- Lombok
- JPA
- Spring Boot
- Tomcat embebido de Spring Boot


DB
El sistema utiliza una Baser de Datos en mamoria "H2"
Las tablas se crean automáticamente al inicio del sistema
Los datos de prueba se insertan automáticamente al inicio del sistema
Tanto tablas como datos son reiniciados a su valor cada vez que inicia el Sistema


TEST MANUAL (postman)
Para realizar las pruebas no se requiere más que importar el proyecto e iniciarlo como una aplicacion de Spring Boot
en la carpeta /postman se incluye una collection para poder probar los distintos endpoint


TEST UNITARIOS (MAVEN)
se pueden correr con "mvn clean test"
OBSERVACION: Los tes unitarios están creado para verificar la DB en memoria, si está se modifica entonces los test probablemente no van a terminar correctamente.



NOTA:
- Endpoint para obtener Conductores en un radio de 30 Kms dada una ubicacion.
Para resolver esto se dejó el sistema preparado pero es algo que debería resolverse a nivel de DB. Para las pruebas se implementó un método HACK que devuelve aleatoreamente 3 conductores disponibles

- Endpoint para obtener Conductores en cercanos a un pasagero
IDEM al metodo anterior
